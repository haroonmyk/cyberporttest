# Response to additional questions #


### The test data contains a big amount of test data. If you would have an ability to put this to the repository, how would you structure it? ###

* The way content repository structured will impact performance and it is recommended that number of child nodes under a single node should not exceed 1000. Keeping that in mind, I would try to decompose the data into many levels. For example, under 'test/filmEntryContainer' resource type content node, child nodes with years (2018, 1964 etc) can be created. Then under each year respective film entries with 'test/filmEntry' resourcetype content can be structured. This would be the suitable solution to structure the data in this case. If this also exceeds, then next level can be created with alphabets and so on.

### How can you improve the performance of the functionality that you're going to implement? ###

* Once the data is loaded into actual AEM instance, then first thing I would do is to reduce the additional iteration of resources children by using use TidyJsonItemWriter api as commented in the code. Since adaptTo Node wasn't supported with Mock Resource, this is not currently implemented.
* If the resource is in AEM instance, then we can leverage Sling Exporter framework api also to get this functionality implemented.
* Also many caching solutions like Google's Guava api can be implemented to cache the results and there by improving performance.
