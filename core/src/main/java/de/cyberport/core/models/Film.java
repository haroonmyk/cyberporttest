package de.cyberport.core.models;

/**
 * 
 */
public class Film {
    
    private String title;
    private String year;
    private Integer awards;
    private Integer nominations;
    private Boolean isBestPicture;
    private Integer numberOfReferences;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getAwards() {
        return awards;
    }

    public void setAwards(Integer awards) {
        this.awards = awards;
    }

    public Integer getNominations() {
        return nominations;
    }

    public void setNominations(Integer nominations) {
        this.nominations = nominations;
    }

    public Boolean getIsBestPicture() {
        return isBestPicture;
    }

    public void setIsBestPicture(Boolean isBestPicture) {
        this.isBestPicture = isBestPicture;
    }
}