package de.cyberport.core.servlets;

import javax.servlet.Servlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;

import de.cyberport.core.models.Film;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.stream.Collectors;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import org.apache.commons.lang3.StringUtils;

/**
 * Servlet that writes information about the Oscar films in json format into the response.
 * It is mounted for all resources of a specific Sling resource type.
 *
 * Based on the request parameters, a filtering and sorting should be applied.
 *
 * For cases when there is no supported request parameter provided in the request,
 * the servlet should return all the films below the requested container.
 *
 * The Servlet must support following request parameters:
 * 1. title - String. The exact film title
 * 2. year - Integer. The exact year when the film was nominated
 * 3. minYear - Integer. The minimum value of the year for the nominated film
 * 4. maxYear - Integer. The maximum value of the year for the nominated film
 * 5. minAwards - Integer. The minimum value for number of awards
 * 6. maxAwards - Integer. The maximum value for number of awards
 * 7. nominations - Integer. The exact number of nominations
 * 8. isBestPicture - Boolean. True to return only the winners of the best picture nomination.
 * 9. sortBy - Enumeration. Sorting in ascending order, supported values are: title, year, awards, nominations.
 *
 * Please note:
 * More then 1 filter must be supported.
 * The resulting JSON must not contain "jcr:primaryType" and "sling:resourceType" properties
 *
 * Examples based on the data stored in oscars.json in resources directory.
 *
 * 1. Request parameters: year=2019&minAwards=4
 *
 * Sample response:
 * {
 *   "result": [
 *     {
 *       "title": "Parasite",
 *       "year": "2019",
 *       "awards": 4,
 *       "nominations": 6,
 *       "isBestPicture": true,
 *       "numberOfReferences": 8855
 *     }
 *   ]
 * }
 *
 * 2. Request parameters: minYear=2018&minAwards=3&sortBy=nominations&limit=4
 *
 * Sample response:
 * {
 *   "result": [
 *     {
 *       "title": "Bohemian Rhapsody",
 *       "year": "2018",
 *       "awards": 4,
 *       "nominations": 5,
 *       "isBestPicture": false,
 *       "numberOfReferences": 387
 *     },
 *     {
 *       "title": "Green Book",
 *       "year": "2018",
 *       "awards": 3,
 *       "nominations": 5,
 *       "isBestPicture": true,
 *       "numberOfReferences": 2945
 *     },
 *     {
 *       "title": "Parasite",
 *       "year": "2019",
 *       "awards": 4,
 *       "nominations": 6,
 *       "isBestPicture": true,
 *       "numberOfReferences": 8855
 *     },
 *     {
 *       "title": "Black Panther",
 *       "year": "2018",
 *       "awards": 3,
 *       "nominations": 7,
 *       "isBestPicture": false,
 *       "numberOfReferences": 770
 *     }
 *   ]
 * }
 *
 * @author Vitalii Afonin
 */
@Component(service = { Servlet.class }, immediate = true)
@SlingServletResourceTypes(
        resourceTypes="test/filmEntryContainer",
        methods=HttpConstants.METHOD_GET,
        extensions="json")
@ServiceDescription("Oscar Film Container Servlet")
public class OscarFilmContainerServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;
    
    public static final String TITLE = "title";
    public static final String YEAR = "year";
    public static final String MIN_YEAR = "minYear";
    public static final String MAX_YEAR = "maxYear";
    public static final String MIN_AWARDS = "minAwards";
    public static final String MAX_AWARDS = "maxAwards";
    public static final String AWARDS = "awards";
    public static final String NOMINATIONS = "nominations";
    public static final String IS_BEST_PICTURE = "isBestPicture";
    public static final String SORT_BY = "sortBy";
    public static final String LIMIT = "limit";

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) {
        //printEntries(req);
        printOscarEntries(req);
    }

    //TODO: remove this method once your check is finished
    /*private void printEntries(SlingHttpServletRequest req) {
        final Resource resource = req.getResource();
        for (Resource child : resource.getChildren()) {
            System.out.println("Entry: " + child.getValueMap());
        }
    }*/
    
    private void printOscarEntries(SlingHttpServletRequest req) {
        final Resource resource = req.getResource();
        
        String title = StringUtils.defaultString(req.getParameter(TITLE));
        String year = StringUtils.defaultString(req.getParameter(YEAR));
        String minYear = StringUtils.defaultString(req.getParameter(MIN_YEAR));
        String maxYear = StringUtils.defaultString(req.getParameter(MAX_YEAR));
        Integer minAwards = StringUtils.isNotEmpty(req.getParameter(MIN_AWARDS)) ? Integer.valueOf(req.getParameter(MIN_AWARDS)) : null;
        Integer maxAwards = StringUtils.isNotEmpty(req.getParameter(MAX_AWARDS)) ? Integer.valueOf(req.getParameter(MAX_AWARDS)) : null;
        Integer nominations = StringUtils.isNotEmpty(req.getParameter(NOMINATIONS)) ? Integer.valueOf(req.getParameter(NOMINATIONS)) : null;
        String isBestPicture = StringUtils.defaultString(req.getParameter(IS_BEST_PICTURE));
        String sortBy = StringUtils.defaultString(req.getParameter(SORT_BY));
        Integer limit = StringUtils.isNotEmpty(req.getParameter(LIMIT)) ? Integer.valueOf(req.getParameter(LIMIT)) : null;
        
        /*
        * Since it is a Mock Resource, adaptTo Node will return null. 
        * Otherwise TidyJsonItemWriter was a better solution to convert to JSON Object
        */
        /*if (resource != null) {
            Node node = resource.adaptTo(Node.class);
            StringWriter stringWriter = new StringWriter();
            TidyJsonItemWriter jsonWriter = new TidyJsonItemWriter(null);

            try {
                jsonWriter.dump(node, stringWriter, -1);
                JSONObject jsonObject = new JSONObject(stringWriter.toString());
            } catch (Exception e) {
                System.out.println("Exception - " + e.getMessage());
            }
        }*/
        
        //since it is mocked resource, have to iterate and create json object

        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        
        List<Film> films = new ArrayList<Film>();
        
        /* Iterating through the resource children can be avoided as commented above, if the resource is loaded in the instance */
        for (Resource child : resource.getChildren()) {
            films.add(gson.fromJson(gson.toJsonTree(child.getValueMap()), Film.class));
        }
        
        List<Film> filteredFilms = films.stream().filter((Film film) -> {
             return
                 (StringUtils.isNotEmpty(title) ? film.getTitle().equals(title) : true) &&
                 (StringUtils.isNotEmpty(year) ? film.getYear().equals(year) : true) &&
                 (StringUtils.isNotEmpty(minYear) ? Integer.valueOf(film.getYear()) >= Integer.valueOf(minYear) : true) &&
                 (StringUtils.isNotEmpty(maxYear) ? Integer.valueOf(film.getYear()) <= Integer.valueOf(maxYear) : true) &&
                 (minAwards != null ? film.getAwards() >= minAwards : true) &&
                 (maxAwards != null ? film.getAwards() <= maxAwards : true) &&
                 (nominations != null ? film.getNominations().equals(nominations) : true) &&
                 (StringUtils.isNotEmpty(isBestPicture) ? film.getIsBestPicture().equals(Boolean.parseBoolean(isBestPicture)) : true);
        }).collect(Collectors.toList());

        
        if (sortBy.equals(TITLE)) {
            filteredFilms.sort(Comparator.comparing(Film::getTitle));
        } else if (sortBy.equals(YEAR)) {
            filteredFilms.sort(Comparator.comparing(Film::getYear));
        } else if (sortBy.equals(AWARDS)) {
            filteredFilms.sort(Comparator.comparing(Film::getAwards));
        } else if (sortBy.equals(NOMINATIONS)) {
            filteredFilms.sort(Comparator.comparing(Film::getNominations));
        }
        
        if (limit != null && filteredFilms.size() > limit) {
            filteredFilms = filteredFilms.subList(0, limit);
        }
        
        System.out.println("Output JSON -> " + gson.toJson(filteredFilms));
        
    }
}